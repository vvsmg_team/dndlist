function spinnerOn(){
    document.querySelector("#spinner").removeAttribute("hidden");
}

function spinnerOff(){
    document.querySelector("#spinner").setAttribute('hidden', '');
}

class ElementId {
    listElementIdCounter = 0;
    generateElementId(){
        return "listElementId" + this.listElementIdCounter++;
    }   
}

const params = new URLSearchParams(window.location.search);

const monsterUrl = "http://www.dnd5eapi.co/api/monsters/" + params.get("index");

const elementIdGenerator = new ElementId();

spinnerOn();
const getMonsterDescription = fetch(monsterUrl)
    .then(response => response.json())
    .then(data => {
        console.log(data);
        displayDescription(data);
    })
    .catch(error => {
        if (error.message === "Failed to fetch") {
            const body = document.querySelector("body");
            body.insertAdjacentHTML("beforebegin","Соси писос, ты вне сети!");
        }
    }).finally(() => spinnerOff());



function displayDescription(data, elementId = "#monsterDescription"){
    const queriedElementId = document.querySelector(elementId);
    if (Array.isArray(data)) {
        if (!data) {

        } else if (typeof data[0] === 'string' || typeof data[key] === 'number'){    
            queriedElementId.insertAdjacentHTML('beforeend', `<li>${data.join(', ')}</li>`) 
        }else {
            displayDescription({...data}, elementId);
            // data.forEach(element => displayDescription(element, level));
        }       
    } else if (typeof data === 'object') {
        let value = '';
        for (key in data) {   
            value = data[key];      
            if (!value) {

            } else if (typeof value === 'string' || typeof value === 'number') {
                queriedElementId.insertAdjacentHTML("beforeend", `<li>${key}: ${value}</li>`);
            } else if (typeof value === 'object') {
                let queriedChildElementId = elementIdGenerator.generateElementId();
                queriedElementId.insertAdjacentHTML("beforeend", `<li>${key}:</li>\n<ul id="${queriedChildElementId}"></ul>`);
                displayDescription(value, '#' + queriedChildElementId);
            }
        }
    }
}
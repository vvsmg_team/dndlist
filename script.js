function spinnerOn(){
    document.querySelector("#spinner").removeAttribute("hidden");
}

function spinnerOff(){
    document.querySelector("#spinner").setAttribute('hidden', '');
}

spinnerOn();
const monsterList = fetch("http://www.dnd5eapi.co/api/monsters/")
    .then(response => response.json())
    .then(data => {
        printList(data.results);
    }).catch(error => {
        console.error(error);
        if (error.message === "Failed to fetch") {
            const body = document.querySelector("body");
            body.insertAdjacentHTML("beforebegin","Соси писос, ты вне сети!");
        }
    }).finally(() => spinner.off())

function printList(data){
    const monsterListTag = document.querySelector("#monsterList");
    data.forEach(element => {
        monsterListTag.insertAdjacentHTML(
            "beforeend",
            `<li><a href="./monster.html?index=${element.index}" target="_blank">${element.name}</a></li>`
        );
    });
}